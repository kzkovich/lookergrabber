package SpringApplication.service;

import SpringApplication.model.Product;

import java.util.List;

public interface ProductService {
    // TODO: 30.05.2020 Добавить: Если материал найден - проверить цену, если разная - обновить, если нет - не изменять
    // TODO: 30.05.2020 Добавить: дату и время создания, дату и время обновления

    //TODO: Добавить логику для работы с IkeaProduct и PackageItem
    //TODO: Инициализировать базу с UUID и связями

    /**
     * Создаем новый товар
     * @param product - товар для создания
     */
    void create(Product product);

    /**
     * Возвращает список всех имеющихся товаров
     */
    List<Product> readAll();

    /**
     * Возвращает товар по его ID
     * @param id - ID товара
     * @return - объект товара с заданным ID
     */
    Product read(long id);

    /**
     * Обновляет товар с заданным ID,
     * в соответствии с переданным товаром
     * @param product - товар с обновленными данными
     * @param id - ID товара, который нужно обновить
     * @return - true если данные были обновлены, иначе false
     */
    boolean update(Product product, long id);

    /**
     * Удаляет товар с заданным ID
     * @param id - ID товара для удаления
     * @return - true если товар был удален, иначе false
     */
    boolean delete(long id);
}
