package SpringApplication;

import Parsing.service.ParselableCategory;
import SpringApplication.service.ProductService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class Application {

    public static final String URL_MATERIALS = "https://www.castorama.pl/produkty/budowa/drzwi-wewnetrzne/drzwi.html";

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ProductService productService) {
        return args -> {
//            startParcing(productService);
            System.out.println("Parsing.Main started");
        };
    }

    public void startParcing(ProductService productService) throws IOException {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        WebDriver driver = new ChromeDriver(options);
        ParselableCategory psp = new ParselableCategory(URL_MATERIALS, driver, productService);
        psp.parseMaterialsAllPages();
        driver.close();
    }


}
