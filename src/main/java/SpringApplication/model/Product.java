package SpringApplication.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private double price;
    private String urlToCategory;
    private String urlToProduct;

    public String getUrlToCategory() {
        return urlToCategory;
    }

    public void setUrlToCategory(String urlToCategory) {
        this.urlToCategory = urlToCategory;
    }

    public String getUrlToProduct() {
        return urlToProduct;
    }

    public void setUrlToProduct(String urlToProduct) {
        this.urlToProduct = urlToProduct;
    }

    protected Product(){}

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format("Product[id=%d, name='%s', price=%e]",
                id, name, price);
    }
}
