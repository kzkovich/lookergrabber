package Parsing.service;

import SpringApplication.model.Product;
import SpringApplication.service.ProductService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class ParselableCategory {

    public String URL_TO_PARSE;
    private int matCount = 0;
    private int totalPages = 0;
    private String URL_TO_POST = "http://localhost:8080/products";
    URL urlToPost;
    Boolean parsed;
    WebDriver webDriver;
    private ProductService productService;

    ParselableCategory() {
    }

    public ParselableCategory(String url, WebDriver driver, ProductService productService) throws IOException {
        this.urlToPost = new URL(URL_TO_POST);
        this.URL_TO_PARSE = url;
        this.webDriver = driver;
        this.productService = productService;
        this.totalPages = countTotalPages();
        this.parsed = false;
    }

    private int countTotalPages() {
        webDriver.get(URL_TO_PARSE + "?p=10000");
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            System.out.println("Поймал ошибку в методе countTotalPages - " + e);
        }
        String[] currUrl = webDriver.getCurrentUrl().split("p=");
        return Integer.parseInt(currUrl[1]);
    }

    @Bean
    private Boolean getMaterialNamePrice(Document doc) {
        Elements materialCards = doc.select("[itemprop='itemListElement']");
        System.out.println("Найдено количество - " + materialCards.size());
        for (Element material : materialCards) {
            ++matCount;
            String price = material.select("[itemprop='price']").attr("content");
            String title = material.select("h3 > a").attr("title");
            String urlToProduct = material.select("h3 > a").attr("href");
            Product product = new Product(title, Double.parseDouble(price));
            product.setUrlToCategory(URL_TO_PARSE);
            product.setUrlToProduct(urlToProduct);
            productService.create(product);
            System.out.println(product.getId() + ". " + product.getName() + " - " + product.getPrice());
        }
        return true;
    }

    public void parseMaterialsAllPages() {
        for (int page = 1; page <= totalPages; page++) {
            webDriver.get(URL_TO_PARSE + "?p=" + page);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                System.out.println("Поймал ошибку в методе countTotalPages - " + e);
            }
            Document doc = Jsoup.parse(webDriver.getPageSource());
            parsed = getMaterialNamePrice(doc);
        }
    }

    public int getParsedCount() {
        return this.matCount;
    }

    public int getTotalPages() {
        return this.totalPages;
    }
}