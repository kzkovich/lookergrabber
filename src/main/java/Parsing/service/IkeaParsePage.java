package Parsing.service;

import Parsing.model.IkeaProduct;
import Parsing.model.PackageItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IkeaParsePage {

    String ikeaProductUrl;
    IkeaProduct product;
    List<PackageItem> packageItem;

    //TODO: Сохранение IkeaProduct в базу, через репозиторий
    public IkeaParsePage(String ikeaProductUrl) {
        this.ikeaProductUrl = ikeaProductUrl;
        Document doc = null;
        try {
            doc = Jsoup.connect(ikeaProductUrl).get();
        } catch (IOException e) {
            System.out.println("Ошибка при попытке подключиться к странице - " + ikeaProductUrl + ". Текст ошибки: " + e);
        }
        this.product = parseProduct(doc);
        this.packageItem = parsePackage(doc);
        this.product.setPackageItems(packageItem);
    }

    public IkeaProduct parseProduct(Document doc) {
        Element productTitle = doc.getElementsByClass("product-pip__name").first();
        Element productPrice = doc.getElementsByClass("product-pip__price__value").first();
        Element vendorCode = doc.select("div#pip_product_description > div > p > span > span").first();
        String urlToImage = doc.select("div.range-carousel__image > img").attr("src");
        return new IkeaProduct(productTitle.text(), urlToImage, productPrice.text(), vendorCode.text());
    }

    public List<PackageItem> parsePackage(Document doc) {
        packageItem = new ArrayList<>();
        Elements packageItems = doc.select("div#pip_package_details > div > div");
        for (Element item : packageItems) {
            String vendorCode = item.select("div.range-expandable__paragraf > div > p > span > span").first().text();
            String name = item.select("div.range-expandable__paragraf > div > h5").first().text();
            String smallCharacteristic = item.select("div.range-expandable__paragraf > div > p.no-margin").first().text();
            List<String> packageCount = item.select(
                    "div.range-expandable__paragraf > div > p.range-expandable__subparagraf > span:matchesOwn(\\d+)").eachText();
            packageItem.add(new PackageItem(name, smallCharacteristic, vendorCode, packageCount, product));
        }

        return packageItem;
    }

    public String getProduct() {
        return product.toString();
    }

//    public String getPackageItem() {
//        String items = "";
//        for (PackageItem item : packageItem) {
//            items = items.concat("\n " + item.toString());
//        }
//        return items;
//    }

    public void setProduct(IkeaProduct product) {
        this.product = product;
    }
}
