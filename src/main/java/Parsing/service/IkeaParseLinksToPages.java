package Parsing.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.lang.model.util.Elements;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class IkeaParseLinksToPages {
    String urlWhereToParseUrls;
    Set<String> urlToParse;

    int numberOfPages;
    int numberOfResults;

    public IkeaParseLinksToPages(String urlWhereToParseUrls) {
        this.urlWhereToParseUrls = urlWhereToParseUrls;
        Document doc = null;
        try {
            doc = Jsoup.connect(this.urlWhereToParseUrls).get();
        } catch (IOException e) {
            System.out.println("Не могу собрать ссылки на товары. Ошибка: " + e);
        }
        WebDriver driver = new ChromeDriver();
        this.numberOfResults = getNumberOfResults(driver);
        this.numberOfPages = getNumberOfPages(numberOfResults);
        this.urlToParse = getUrlsToParse(numberOfPages, driver);
    }

    private int getNumberOfResults(WebDriver driver) {
        driver.get(urlWhereToParseUrls);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            System.out.println("Поймал ошибку в методе getNumberOfResults - " + e);
        }
        Document doc = Jsoup.parse(driver.getPageSource());
        Element totalCount = doc.select("div.catalog-filter__total > span > span").first();
        return Integer.parseInt(totalCount.ownText());
    }

    private int getNumberOfPages(int numberOfResults) {
        return (int) Math.ceil(numberOfResults / 24);
    }

    public Set<String> getUrlsToParse() {
        return urlToParse;
    }

    private Set<String> getUrlsToParse(int numberOfPages, WebDriver driver) {
        Document doc = null;
        List<String> list = new ArrayList<>();
        driver.get(urlWhereToParseUrls + "?page=" + (numberOfPages + 1));
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            System.out.println("Поймал ошибку в методе getNumberOfResults - " + e);
        }
        doc = Jsoup.parse(driver.getPageSource());
        list.addAll(doc.select("div.product-compact__spacer > a").eachAttr("href"));
        return new LinkedHashSet<>(list);
    }

    public String compareResultWithFact() {
        return "На странице результатов " + numberOfResults + ". Ссылок найдено " + urlToParse.size();
    }

    @Override
    public String toString() {
        String listOfUrls = "Ссылки найдены:\n";
        for(String url : urlToParse) {
            listOfUrls = listOfUrls.concat("\n" + url);
        }
        return listOfUrls;
    }
}
