package Parsing.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "IkeaProduct")
@Table(name = "ikea_product")

public class IkeaProduct {
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private String price;
    @Column(name = "vendor_code")
    private String vendorCode;
    @Column(name = "url_to_image")
    private String urlToImage;

    public void setPackageItems(List<PackageItem> packageItems) {
        this.packageItems = packageItems;
    }

    @OneToMany(
            mappedBy = "ikeaProduct",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<PackageItem> packageItems;

    public IkeaProduct(){}

    public IkeaProduct(String name, String urlToImage, String price, String vendorCode) {
        this.name = name;
        this.price = price;
        this.vendorCode = vendorCode;
        this.urlToImage = urlToImage;
    }

    @Override
    public String toString() {
        String textProduct = "Название - " + name + ". Цена - " + price + ". Артикул - " + vendorCode + "\nСсылка на изображение - " + urlToImage;
        String textPackageItems = "\nСостоит из:\n";
        for (PackageItem item : packageItems) {
            textPackageItems = textPackageItems.concat(item.toString() + "\n");
        }
        return textProduct + textPackageItems;
    }

}
