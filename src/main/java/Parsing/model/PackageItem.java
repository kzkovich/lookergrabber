package Parsing.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "package_item")
public class PackageItem {
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;
    private String name;
    private String smallCharacteristic;
    private String vendorCode;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ikea_product_id", nullable = false)
    private IkeaProduct ikeaProduct;

    public void setIkeaProduct(IkeaProduct ikeaProduct) {
        this.ikeaProduct = ikeaProduct;
    }

    public IkeaProduct getIkeaProduct() {
        return ikeaProduct;
    }

    int packageCount = 0;

    public PackageItem() {}

    public PackageItem(String name, String smallCharacteristic, String vendorCode, List<String> packageCountArray, IkeaProduct ikeaProduct) {
        this.name = name;
        this.smallCharacteristic = smallCharacteristic;
        this.vendorCode = vendorCode;
        this.packageCount = getPackageCount(packageCountArray);
        this.ikeaProduct = ikeaProduct;
//        this.contents = contents;
    }

    int getPackageCount(List<String> packageCountArray) {
        int count = 0;
        for (String pack : packageCountArray) {
            count += Integer.parseInt(pack);
        }
        return count;
    }

    @Override
    public String toString() {
        return "Имя: " + name + ". Категория: " + smallCharacteristic + ". Кол-во пачек: " + packageCount + ". Артикул: " + vendorCode;
    }

}
