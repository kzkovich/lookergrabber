package Parsing;

import Parsing.service.IkeaParseLinksToPages;
import Parsing.service.IkeaParsePage;

import java.util.*;

public class IkeaMain {

    public static final String URL_TO_PARSE = "https://www.ikea.com/pl/pl/p/pax-szafa-imitacja-okleiny-bejc-na-braz-forsand-imitacja-okleiny-bejc-na-braz-s09287615/";
    public static final String URL_TO_PARSE_URL = "https://www.ikea.com/pl/pl/cat/dywany-10653/";

    public static void main(String[] args) {
        List<IkeaParsePage> iPP = new ArrayList<>();
        IkeaParseLinksToPages iPLTP = new IkeaParseLinksToPages(URL_TO_PARSE_URL);
        for (String url : iPLTP.getUrlsToParse()) {
            IkeaParsePage cPP = new IkeaParsePage(url);
            iPP.add(cPP);
        }
        System.out.println(iPLTP.toString());
        for (IkeaParsePage page : iPP) {
            System.out.println(page.getProduct());
        }
        System.out.println(iPLTP.compareResultWithFact());
    }

}
