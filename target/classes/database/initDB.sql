CREATE TABLE IF NOT EXISTS products
(
    id      BIGSERIAL PRIMARY KEY ,
    name    VARCHAR(200) NOT NULL ,
    price   REAL NOT NULL
);